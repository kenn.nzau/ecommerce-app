const CurrencyActionTypes = {
  UPDATE_CURRENCIES: "UPDATE_CURRENCIES",
  SET_CURRENCY: "SET_CURRENCY",
};

export default CurrencyActionTypes;
